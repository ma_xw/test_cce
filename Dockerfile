FROM python:slim
RUN pip install flask
RUN mkdir -p /app/flask_hello
COPY ./ /app/flask_hello
WORKDIR /app/flask_hello
EXPOSE 5001
CMD ["python3","flask-hello.py"]
